# Tests for the no_python group (pip and docker Python library not installed)
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('no_python')


def test_pip_not_installed(host):
    assert not host.file('/usr/bin/pip').exists
