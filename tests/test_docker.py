# Generic docker tests
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_docker_running_and_enabled(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled


def test_docker_run(host):
    with host.sudo():
        cmd = host.command('docker run hello-world')
    assert cmd.rc == 0
    assert 'Hello from Docker!' in cmd.stdout
